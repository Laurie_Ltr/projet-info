from abc import ABC, abstractmethod
from Table import Table

class Transformation(ABC) :
    def __init__(self, table):
        self.table = table

    @abstractmethod
    def __str__(self):
        pass