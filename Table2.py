from numpy import delete


class Table :
    def __init__(self, en_tete, corps):
        self.__en_tete = en_tete
        self.__corps = corps

    def get_en_tete(self):
        return(self.__en_tete)

    def get_corps(self):
        return(self.__corps)

    def ajouter_colonne(self, colonne):
        self.__en_tete.append(colonne.__en_tete[0])
        for i in range(len(self.__corps)):
            self.__corps[i].append(colonne.__corps[i])
    
    def ajouter_ligne(self, ligne):
        self.__corps.append(ligne)
    
    def supprimer_colonne(self, colonne):
        for i in colonne:
            indices_var = []
            j = 0
            while j < len(self.__en_tete):
                if self.__en_tete[j] == i:
                    indices_var.append(j)
                else:
                    j = j + 1
        for k in indices_var:
            self.__en_tete.drop(k)
            self.__corps.drop(k)

    def supprimer_ligne(self, ligne):
        for i in range(len(self.__corps)):
            if test.get_corps()[i] == ligne:
                delete(self.__corps, i)
    
    def __str__(self):
        var = ""
        for i in range(len(self.__corps)):
            var += "\n" + str(self.__corps[i])
        return "La table obtenue est :" + "\n" + str(self.__en_tete) + var


test = Table(["Nom", "Prénom"], [["Leterrier", "Laurie"], ["Lejas", "Jules"]])
#print(test.get_en_tete())
#print(test.get_corps())
colonne = Table(["Âge"], [21, 21])
test.ajouter_colonne(colonne)
#print(test)
ligne = ["Lebrun", "Justine", 22]
test.ajouter_ligne(ligne)
#print(test)
test.supprimer_ligne(['Lejas', 'Jules', 21])
print(test.get_corps()[1])


