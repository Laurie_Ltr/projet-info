class Table :
    def __init__(self, en_tete, corps):
        self.en_tete = en_tete
        self.corps = corps

    def ajouter(self, colonne):
        self.en_tete.append(colonne.en_tete)
        for i in range(len(self.corps)):
            self.corps[i].append(colonne.corps[i])
    
    def supprimer(self, variable):
        for i in variable:
            indices_var = []
            j = 0
            n = 0
            while j < len(self.en_tete) and n == 0:
                if self.en_tete[j] == i:
                    indices_var.append(j)
                else:
                    j = j + 1
        for k in indices_var:
            self.en_tete.drop(k)
            self.corps.drop(k)
    
    def __str__(self):
        return "La table obtenue est"