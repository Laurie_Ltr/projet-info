import json

class  JSON:
    def __init__(self, fichier, separateur, dictionnaire, mode):
        self.fichier = fichier
        self.separateur = separateur
        self.dictionnaire = dictionnaire
        self.mode = mode

    def importer(self):
        with open("{}.json".format(self.fichier),"{}".format(self.mode)) as mon_fichier:           #y'a t'il un mode à utiliser pour l'import ?
        data = json.load(mon_fichier)
        print(data)

    def exporter(self):
        out_file = open("{}.json".format(self.fichier), "{}".format(self.mode))  
        json.dump(self.dictionnaire, out_file, indent = 6)  
        out_file.close() 
    
dict1 ={  
    "emp1": {  
        "name": "Lisa",  
        "designation": "programmer",  
        "age": "34",  
        "salary": "54000"
    },  
    "emp2": {  
        "name": "Elis",  
        "designation": "Trainee",  
        "age": "24",  
        "salary": "40000"
    },  
}  
    
