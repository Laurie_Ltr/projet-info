from Transformation import Transformation
from Table import Table
import numpy as np

class Selection(Transformation):
    def __init__(self, table, variable):
        super().__init__(table)
        self.variable = variable

    def get_colonne(self,nom):
        indice = self.table.get_en_tete().index(nom)
        nom_colonne = nom
        corps = []
        for i in range(0,len(self.table.get_en_tete())):
            corps += [self.table.get_corps()[i][indice]]
        return(nom_colonne, corps)

    def selectionner(self):
        Tete = []
        Corps = []
        for j in self.variable :
            colonne = self.get_colonne(j)
            Tete.append(colonne[0])
            Corps.append(colonne[1]) 
        return(Table(Tete, Corps))
    
    def __str__(self):
        return "La table obtenue est :{}".format(self.table.selectionner())
